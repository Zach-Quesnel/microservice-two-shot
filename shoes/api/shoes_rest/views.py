from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
import json

from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVOEncoder()
    }



@require_http_methods(["GET", "POST"])
def shoe_list(request):

    if request.method == "GET":
        shoes = Shoe.objects.all()
        shoes_data = [ShoeListEncoder().default(shoe) for shoe in shoes]
        return JsonResponse(
            shoes_data,
            safe=False
        )
    else: # POST
        content = json.loads(request.body)
        try:
            bin_href = f'/api/bins/{content["bin"]}/'
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"]  = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin ID"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def shoe_detail(request, pk):
    shoe = get_object_or_404(Shoe, pk=pk)
    if request.method == "GET":
        return JsonResponse(
            ShoeDetailEncoder().default(shoe),
            safe=False,
        )
    else: # DELETE
        shoe.delete()
        return JsonResponse({}, status=204)
