from django.urls import path
from .views import shoe_list, shoe_detail

urlpatterns = [
    path('shoes/', shoe_list, name='list_shoes'),
    path('shoes/<int:pk>/', shoe_detail, name='shoe_detail'),
]
