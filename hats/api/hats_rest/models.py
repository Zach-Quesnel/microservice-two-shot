from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url= models.URLField(max_length=400, null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name = "hats",
        on_delete=models.PROTECT,
        null=True,
    )

    def __str__(self):
        return f'self.style_name'

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"id": self.id})

    # class Meta:
    #     ordering = ("starts", "name")  # Default ordering for Conference
