# Wardrobify

Team:

* (Zach Quesnel) - Hats Microservice
* (Miguel Robles) - shoes microservice

## Design

## Shoes microservice

I used two models, Shoe and BinVO, to store different info. The Shoe model keeps track of the manufacturer, model name, color, a picture URL, and which bin the shoe is in. The BinVO model stores info about the storage bin, with details like closet name, bin number, size, and a URL for data. This is derived from the Bin model in the wardrobe microservice.

This microservice has API endpoints for listing, creating, and deleting shoes. On the frontend, I used React components like ShoeList, ShoeForm, and ShoeDetail to communicate with the backend that I worked on beforehand, making it easy for users to manage their shoe collection and storage bins.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The hats models have different characteristics (fabric, style, color, picture) that exist in the hats microservice. The poller then reaches out to the wardrobe microservice to access location data and adds it as an object to the hats data.
