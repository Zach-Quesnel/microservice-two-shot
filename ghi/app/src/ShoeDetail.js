import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';


function ShoeDetail() {
  const { id } = useParams();
  const [shoe, setShoe] = useState(null);

  useEffect(() => {
    fetch(`http://localhost:8080/api/shoes/${id}/`)
      .then((response) => response.json())
      .then((data) => setShoe(data));
  }, [id]);

  if (!shoe) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h2 className="mb-4">{shoe.manufacturer} {shoe.model_name}</h2>
      <p>Color: {shoe.color}</p>
      <img src={shoe.picture_url} alt={`${shoe.manufacturer} ${shoe.model_name}`} className="img-fluid mb-3" />
      <p>(Size: {shoe.bin.bin_size})</p>
    </div>
  );
}

export default ShoeDetail;
