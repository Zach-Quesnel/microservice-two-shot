import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ShoeList() {
  const [shoes, setShoes] = useState([]);

  useEffect(() => {
    fetchShoes();
  }, []);

  const fetchShoes = () => {
    fetch('http://localhost:8080/api/shoes/')
      .then((response) => response.json())
      .then((data) => setShoes(data));
  };

  const deleteShoe = (shoeId) => {
    fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
      method: 'DELETE',
    })
      .then((response) => {
        if (response.status === 204) {
          fetchShoes();
        } else {
          console.error('Error deleting shoe:', response);
        }
      });
  };

  return (
    <div>
      <h2>Shoes</h2>
      <div className="row">
        {shoes.map((shoe, index) => (
          <div key={index} className="col-md-4">
            <div className="card mb-4">
            <Link to={`/shoes/${shoe.id}/`}>
              <img src={shoe.picture_url} alt={`${shoe.manufacturer} ${shoe.model_name}`} className="img-thumbnail" />
            </Link>
            <div className="card-body">
              <h5 className="card-title">{shoe.manufacturer} {shoe.model_name}</h5>
              <p className="card-text">Color: {shoe.color}</p>
              <button className="btn btn-danger" onClick={() => deleteShoe(shoe.id)}>Delete</button>
            </div>
          </div>
        </div>
      ))}
      </div>
    </div>
  );
}

export default ShoeList;
