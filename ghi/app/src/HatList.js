import React, { useState, useEffect } from 'react';



function HatList(props) {
  const [hats, setHats] = useState([]);

  const handleDelete = async (value) => {
    const hatUrl = `http://localhost:8090/api/hats/${value.id}/`
    const fetchConfig = {
      method: "delete",
    }
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      getHats();
      console.log("Delete Successful");
    }
  }




  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
      console.log('hatssssss', data);
    }
  }


  useEffect(() => {
      getHats();
  }, []);



    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Closet</th>
            <th>Closet Section</th>
            <th>Shelf Number</th>
          </tr>
        </thead>
        <tbody>
          {hats.map((hat, index) => {
            return (
              <tr key={hat.style_name + index}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td> <img src={ hat.picture_url } alt={hat.style_name} className="img-thumbnail" height="70" width="70" /></td>
                <td>{ hat.location.closet_name}</td>
                <td>{ hat.location.section_number }</td>
                <td>{ hat.location.shelf_number }</td>
                <td><button onClick={() => handleDelete(hat)}>Delete</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatList;
