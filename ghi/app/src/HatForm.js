import React, { useState, useEffect } from 'react';

function HatForm(props) {
  const [fabric, setFabric] = useState('');
  const [styleName, setStyleName] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [location, setLocation] = useState('');
  const [locations, setLocations] = useState([]);

  useEffect(() => {
    async function fetchLocations() {
      const url = 'http://localhost:8100/api/locations/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
    }
    fetchLocations();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      fabric: fabric,
      style_name: styleName,
      color: color,
      picture_url: pictureUrl,
      location: location,
    };

    const locationUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      setFabric('');
      setStyleName('');
      setColor('');
      setPictureUrl('');
      setLocation('')
    }
  }

  function handleFabricChange(event) {
    const value = event.target.value;
    setFabric(value);
  }

  function handleStyleNameChange(event) {
    const value = event.target.value;
    setStyleName(value);
  }

  function handleColorChange(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handlePictureUrlChange(event) {
    const value = event.target.value;
    setPictureUrl(value);
  }

  function handleLocationChange(event) {
    const value = event.target.value;
    setLocation(value);
  }



  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input value={styleName} onChange={handleStyleNameChange} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control" />
              <label htmlFor="style_name">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>
                      {location.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );

}

export default HatForm;
