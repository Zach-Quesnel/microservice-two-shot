import React, { useState } from 'react';


function NewShoeForm() {
  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [binId, setBinId] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();
    const newShoe = {
      manufacturer,
      model_name: modelName,
      color,
      picture_url: pictureUrl,
      bin: binId,
    };

    fetch('http://localhost:8080/api/shoes/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newShoe),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.shoe) {
          // Redirect to the shoe detail page or show a success message
          console.log('Shoe created successfully');
        } else {
          console.error('Error creating shoe');
        }
      });
  };

  return (
    <div>
      <h2 className="mb-4">Add New Shoe</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="manufacturer" className="form-label">Manufacturer</label>
          <input type="text" className="form-control" id="manufacturer" value={manufacturer} onChange={(e) => setManufacturer(e.target.value)} required />
        </div>
        <div className="mb-3">
          <label htmlFor="modelName" className="form-label">Model Name</label>
          <input type="text" className="form-control" id="modelName" value={modelName} onChange={(e) => setModelName(e.target.value)} required />
        </div>
        <div className="mb-3">
          <label htmlFor="color" className="form-label">Color</label>
          <input type="text" className="form-control" id="color" value={color} onChange={(e) => setColor(e.target.value)} required />
        </div>
        <div className="mb-3">
          <label htmlFor="pictureUrl" className="form-label">Picture URL</label>
          <input type="text" className="form-control" id="pictureUrl" value={pictureUrl} onChange={(e) => setPictureUrl(e.target.value)} required />
        </div>
        <div className="mb-3">
          <label htmlFor="binId" className="form-label">Bin ID</label>
          <input type="text" className="form-control" id="binId" value={binId} onChange={(e) => setBinId(e.target.value)} required />
        </div>
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
    </div>
  );
}

export default NewShoeForm;
